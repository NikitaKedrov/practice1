package ua.nure.kedrov.practice1;

/**
 * Class that implements the functionality of adding two numbers.
 * @author Nikita Kedrov
 */
public final class Part2 {
	
	/**
	 * Utility class constructor.
	 */
	private Part2() {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Method that implements the functionality of adding two numbers.
	 * @param args - Incoming array of strings
	 * @throws NumberFormatException - Exception if in array is not a number
	 */
	public static void sum(String[] args) {
		if (args.length == 2) {
			try {
				System.out.println(Double.parseDouble(args[0]) + Double.parseDouble(args[1]));  
		    } catch (NumberFormatException e) {
		    	System.out.println("Error, you enter not a number");
		    }
		}else {
			System.out.println("array of arguments length is less than 2");
		}
	}
	
	/**
	 * Entry point.
	 * @param args - command line arguments
	 */
	public static void main(final String[] args) {
		sum(args);
	}

}
