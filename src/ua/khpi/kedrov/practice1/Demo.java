package ua.nure.kedrov.practice1;
/**
 * Class demonstrates the work of all subtasks.
 * @author Nikita Kedrov
 */
public class Demo {
	
	/**
	 * Utility class constructor.
	 */
	private Demo() {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Entry point.
	 * @param args - command line arguments 
	 */
	public static void main(String[] args) {
		System.out.println("Task1:");
		Part1.hello();
		System.out.println("Task2:");
		Part2.main(new String[] {"2","3"});
		System.out.println("Task3:");
		Part3.main(new String[] {"20","30","40"});
		System.out.println("Task4:");
		Part4.main(new String[] {"30","18"});
		System.out.println("Task5:");
		Part5.main(new String[] {"3018"});
		System.out.println("Task6:");
		Part6.main(new String[] {"77"});
		System.out.println("\nTask7:");
		System.out.println("str2int: ATT = " + Part7.str2int("ATT"));
		System.out.println("int2str: 1216 = " + Part7.int2str(1216));
		System.out.println("rightColumn: ATT = " + Part7.rightColumn("ATT"));
		
	}

}
