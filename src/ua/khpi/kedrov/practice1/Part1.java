package ua.nure.kedrov.practice1;

/**
 * Class displays "Hello, World" in console.
 * @author Nikita Kedrov
 */
public final class Part1 {
	
	/**
	 * Utility class constructor.
	 */
	private Part1() {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Method that displays "Hello, World" in console.
	 */
	public static void hello() {
		System.out.println("Hello, World");
	}
	
	/**
	 * Entry point.
	 * @param args - command line arguments
	 */
	public static void main(final String[] args) {
		hello();
	}

}
