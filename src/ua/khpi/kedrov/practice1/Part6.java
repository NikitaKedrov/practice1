package ua.nure.kedrov.practice1;

import java.util.logging.Logger;

/**
 * Class that creates an array of n elements and fills it with an ascending row of prime numbers(n from command line).
 * @author Nikita Kedrov
 */
public final class Part6 {
	
	private static final Logger LOGGER = Logger.getLogger(Part6.class.getName());
	private static int[] array;

	/**
	 * Utility class constructor.
	 */
	private Part6() {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Method that checks a number for simplicity. 
	 * @param number - check number
	 * @return if number is prime return true, else return false
	 */
	public static boolean isPrime(int number) {
		for(int i = 2; i <= number/2; i++) {
			if (number%i == 0) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Method that fill array.
	 * @param args Incoming array of strings
	 * @return array of prime numbers
	 * @throws NumberFormatException - Exception if in array is not a number
	 */
	public static void fillArray(String[] args) {
		int length = Integer.parseInt(args[0]);
		array = new int[length];
		int countForArray = 0;
		for (int i = 2;; i++) {
			if (isPrime(i)) {
				array[countForArray++] = i;
				if(countForArray == length) {
					break;
				}
			}
		}
	}
	
	/**
	 * Entry point.
	 * @param args - command line arguments
	 */
	public static void main(final String[] args) {
		try {
			if(args.length != 0) {
				fillArray(args);
				for(int i = 0; i < array.length; i++) {
					System.out.print(array[i] + " ");
				}
			}else {
				System.out.println("args empty!");
			}
		} catch (NumberFormatException e) {
			LOGGER.log(null, "context", e);
		}
	}
}
