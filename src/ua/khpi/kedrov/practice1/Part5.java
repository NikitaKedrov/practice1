package ua.nure.kedrov.practice1;

import java.util.logging.Logger;

/**
 * Class that implements the functionality of determining the sum of digits of a positive integer.
 * @author Nikita Kedrov
 */
public final class Part5 {
	
	private static final Logger LOGGER = Logger.getLogger(Part5.class.getName());

	/**
	 * Utility class constructor.
	 */
	private Part5() {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Method that implements the functionality of determining the sum of digits of a positive integer.
	 * @param args - Incoming array of strings
	 * @throws NumberFormatException - Exception if in array not a number
	 */
	public static void sumOfDigits(String[] args) {
		try {
			String[] str = args[0].split("");
			int sum = 0;
			for (String s : str) {
				sum += Integer.parseInt(s);
			}
			System.out.println(sum);
		} catch (NumberFormatException e) {
			LOGGER.log(null, "context", e);
		}
	}
	
	/**
	 * Entry point.
	 * @param args - command line arguments
	 */
	public static void main(final String[] args) {
		if (args.length == 0) {
			System.out.println("args empty");
		}else {
			sumOfDigits(args);
		}
	}
}
