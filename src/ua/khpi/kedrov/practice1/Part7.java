package ua.nure.kedrov.practice1;

/**
 * Class implements converts numbers base-10 to base-26 system.
 * @author Nikita Kedrov
 */
public final class Part7 {
	
	private static final String SPECIAL_FOR_SONAR = " ==> ";

	/**
	 * Utility class constructor.
	 */
	private Part7() {
		throw new UnsupportedOperationException();
	}

	/**
	 * Method of determining the serial number of a column by its letter number.
	 * @param number - Initial data (letter number)
	 * @return integer number
	 */
	public static int str2int(String number) {
		int num = 0;
		if (number.length() < 2) {
			num = (int) number.charAt(0);
			return num - 64;
		} else {
			for (int i = 0, j = number.length() - 1; i < number.length(); i++, j--) {
				num += ((int) number.charAt(i) - 64) * Math.pow(26, j);
			}
			return num;
		}
	}

	
	/**
	 * Method for determining the letter number of a column by its ordinal number.
	 * @param number - Initial data (integer number)
	 * @return letter number
	 */
	public static String int2str(int number) {
		StringBuilder result = new StringBuilder();
		while (number > 0) {
			int numberIndex = number % 26;
			number = (number - 1) / 26;
			if (numberIndex == 0) {
				numberIndex = 26;
			}
			numberIndex += 64;
			result.append((char) numberIndex);
		}
		return result.reverse().toString();
	}

	/**
	 * Method of determination by the letter number of the column number of the column to the right of this.
	 * @param number Initial data (letter number)
	 * @return 
	 */
	public static String rightColumn(String number) {
		return int2str((str2int(number) + 1));
	}

	/**
	 * Entry point.
	 * @param args - command line arguments
	 */
	public static void main(final String[] args) {
		System.out.println("A ==> " + str2int("A") + SPECIAL_FOR_SONAR + int2str(str2int("A")));
		System.out.println("B ==> " + str2int("B") + SPECIAL_FOR_SONAR + int2str(str2int("B")));
		System.out.println("Z ==> " + str2int("Z") + SPECIAL_FOR_SONAR + int2str(str2int("Z")));
		System.out.println("AA ==> " + str2int("AA") + SPECIAL_FOR_SONAR + int2str(str2int("AA")));
		System.out.println("AZ ==> " + str2int("AZ") + SPECIAL_FOR_SONAR + int2str(str2int("AZ")));
		System.out.println("BA ==> " + str2int("BA") + SPECIAL_FOR_SONAR + int2str(str2int("BA")));
		System.out.println("ZZ ==> " + str2int("ZZ") + SPECIAL_FOR_SONAR + int2str(str2int("ZZ")));
		System.out.println("AAA ==> " + str2int("AAA") + SPECIAL_FOR_SONAR + int2str(str2int("AAA")));
	}

}
