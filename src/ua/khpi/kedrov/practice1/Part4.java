package ua.nure.kedrov.practice1;

import java.util.logging.Logger;

/**
 * Class that implements the functionality of determining the greatest common divisor of two positive integers.
 * @author Nikita Kedrov
 */
public final class Part4 {
	
	private static final Logger LOGGER = Logger.getLogger(Part4.class.getName());

	/**
	 * Utility class constructor.
	 */
	private Part4() {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Method that implements the functionality of determining the greatest common divisor of two positive integers.
	 * @param args - Incoming array of strings
	 */
	public static void greatestCommonDivisor(String[] args) {
		int firstNumber;
		int secondNumber;
		
		try {
			firstNumber = Integer.parseInt(args[0]);
			secondNumber = Integer.parseInt(args[1]);
			while (firstNumber != secondNumber) {
				if (firstNumber > secondNumber) {
					firstNumber = firstNumber - secondNumber;
				} else {
					secondNumber = secondNumber - firstNumber;
				}
			}
			System.out.println(firstNumber);
		} catch (NumberFormatException e) {
			LOGGER.log(null, "context", e);
		}
	}
	
	/**
	 * Entry point.
	 * @param args - command line arguments
	 */
	public static void main(final String[] args) {
		if (args.length == 2){
			greatestCommonDivisor(args);
		}else {
			System.out.println("args length != 2");
		}
	}

}
