package ua.nure.kedrov.practice1;

/**
 * Class that implements the functionality of displaying command line parameters in the console.
 * @author Nikita Kedrov
 */
public final class Part3 {
	
	/**
	 * Utility class constructor
	 */
	private Part3() {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Method that implements the functionality of displaying command line parameters in the console.
	 * @param array - Incoming array of strings
	 */
	public static void spaceSeparatedOutput(String[] array) {
		if (array.length > 0) {
			StringBuilder result = new StringBuilder();
			for (String string : array) {
				result.append(string + " ");
			}
			System.out.println(result);
		}else {
			System.out.println("args length = 0");
		}
	}
	
	/**
	 * Entry point.
	 * @param args - command line arguments
	 */
	public static void main(final String[] args) {
		spaceSeparatedOutput(args);
	}

}
